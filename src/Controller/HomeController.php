<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        $users = [['lastname'=> 'Dupont', "firstname" => 'Géradd', "age" =>'45'],
                    ['lastname'=> 'Martin', "firstname" => 'Josette', "age" =>'33'],
            ['lastname'=> 'Twig', "firstname" => 'Jude', "age" =>'37'],
            ['lastname'=> 'Jenga', "firstname" => 'Marcel', "age" =>'87']];
        return $this->render('home/home.html.twig', [
            'title' => 'Exercices twig',
            'users' => $users
        ]);
    }

}
