<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class UserController extends AbstractController
{
    /**
     * @Route("/users", name="users")
     */
    public function index()
    {
        $users = [['lastname'=> 'Dupont', "firstname" => 'Géradd', "age" =>'45'],
                    ['lastname'=> 'Martin', "firstname" => 'Josette', "age" =>'33'],
            ['lastname'=> 'Twig', "firstname" => 'Jude', "age" =>'37'],
            ['lastname'=> 'Jenga', "firstname" => 'Marcel', "age" =>'87']];
        return $this->render('user/index.html.twig', [
            'title' => 'Liste des utilisateurs',
            'users' => $users
        ]);
    }

    protected function getUserName($name)
    {
        $users = [['lastname'=> 'Dupont', "firstname" => 'Géradd', "age" =>'45'],
            ['lastname'=> 'Martin', "firstname" => 'Josette', "age" =>'33'],
            ['lastname'=> 'Twig', "firstname" => 'Jude', "age" =>'37'],
            ['lastname'=> 'Jenga', "firstname" => 'Marcel', "age" =>'87']];

        foreach ($users as $index => $user) {
            if($user['lastname'] == $name) {
                return $users[$index];
            }
        }
    }

    /**
     * @Route("/users/{name}", name="user_show")
     */
    public function show($name)
    {
        return $this->render('user/show.html.twig', [
            'user' => $this->getUserName($name),
            'title' => 'page user'
        ]);
    }
}
